package ru.nsu.ccfit.lusnikov.minesweeper;

import ru.nsu.ccfit.lusnikov.minesweeper.Model.Model;

public class TimerThread extends Thread {
    boolean isrun;
    private double time_s;
    private Model model;
    public TimerThread(Model model){
        isrun = false;

        this.model = model;
    }
    @Override
    public void run(){
        time_s = System.nanoTime();
        while(model.getStage() != Model.Stage.ENDGAME) {
            //time = System.nanoTime() - time_s;
            try{
                sleep(1000);
            }
            catch (Exception e){
                //interrupt();
                return;
            }

            model.notify_all_grafic();
        }
    }
    public void set(){
        isrun = true;
        time_s = System.nanoTime();
    }
    public Double getTime(){
        if (isrun) return 1e-9*(System.nanoTime() - time_s);
        else return new Double(0);
    }
    public void dispose(){
        isrun = false;
    }
}
