package ru.nsu.ccfit.lusnikov.minesweeper.Model;

public class HighScoreCell {
    private String name;
    private Float time;
    private Integer mineCount;
    public HighScoreCell(String n, Float t, Integer m){
        name = n;
        time = t;
        mineCount = m;
    }
}
