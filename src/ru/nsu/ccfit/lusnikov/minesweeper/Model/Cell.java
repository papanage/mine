package ru.nsu.ccfit.lusnikov.minesweeper.Model;

public  class Cell {
    private Integer item;
    public enum Mode{
        FLAG,
        HIDE,
        SEE
    }
    private Mode isVisible;
    public  Cell (int it, Mode chr){
        item = it;
        isVisible = chr;
    }
    public void reverseBeh(Mode b){isVisible = b;}
    public Integer getItem() {return  item;}
    public Mode getVisible() {return  isVisible;}
}
