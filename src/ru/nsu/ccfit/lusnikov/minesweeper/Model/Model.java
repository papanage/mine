package ru.nsu.ccfit.lusnikov.minesweeper.Model;

import ru.nsu.ccfit.lusnikov.minesweeper.Observer.Publisher;
import ru.nsu.ccfit.lusnikov.minesweeper.Observer.ModelSubscriber;
import ru.nsu.ccfit.lusnikov.minesweeper.TimerThread;

import java.io.*;
import java.util.*;


public class Model extends Publisher {
    private TimerThread timerThread;
    private byte isWin;
    private Double timeWin;
    private HashMap<String, Double> records = new HashMap<>();
    public HighScores record = new HighScores();
    public enum Stage{
        MENAU,
        ABOUT,
        HIGHSCORE,
        FPUT,
        NPUT,
        EXIT,
        PUTNAME,
        ENDGAME

    }
    public enum MANUE{
        NEW("1 New Game"),ABOUT("2 About"),EXIT("4 Exit"), HIGHSCORE("3 Highscore");
        private String name;
        MANUE(String name){
        this.name = name;
        }
        public String getName(){
            return name;
        }
        public void setName(String name){ this.name = name;}
    }
    public enum BEH{
        FLAGBOMB(-8),
        UNFLAGBOMB(-7),
        FLAG(-5),
        BOMB(-4),
        INVIS(-6),
        NUMBER(1);
        private Integer index;
        BEH(int id){
            index = id;
        }
        public Integer get(){
            return index;
        }
        public BEH set(Integer i){
            index = i;
            return  this;
        }

    }
    private Stage stage;
    private int size;
    private int mineCount;
    private Cell[] field;
    public Model(int s, int m){
        //loadScoreTable();
        timerThread = new TimerThread(this);
        isWin = 0;
        size = s;
        mineCount = m;
        stage = Stage.MENAU;

        field = new Cell[size*size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                field[coord(j,i)] = new Cell(0, Cell.Mode.HIDE);
            }
        }

    }
    //
    public Double getTime(){
        //timerThread.run();
        return timerThread.getTime();
    }
    public BEH getViewOfCell(int x, int y){
        Cell c = field[coord(x,y)];
        if (isWin != 0 && c.getItem() == -1 && c.getVisible() != Cell.Mode.FLAG ) return BEH.UNFLAGBOMB;
        if (isWin != 0 && c.getItem() == -1 && c.getVisible() == Cell.Mode.FLAG ) return BEH.FLAGBOMB;
        if (c.getVisible() == Cell.Mode.SEE)
            if (c.getItem() == -1) return BEH.BOMB;//-4
            else {
                return BEH.NUMBER.set(c.getItem());
            }
        else if (c.getVisible() == Cell.Mode.FLAG) return BEH.FLAG;//-5
        else return BEH.INVIS;//-6
    }
    private int check(){
        int j, count = 0;
        for (int i = 0; i < size; i++) {
            for (j = 0; j < size; j++) {
                if (field[coord(j,i)].getItem() == -1 && field[coord(j,i)].getVisible() == Cell.Mode.FLAG)
                    count++;
                else if (field[coord(j,i)].getItem() != -1 && field[coord(j,i)].getVisible() == Cell.Mode.FLAG)
                    return 2;

            }
        }
        return count - mineCount;
    }
    public void addRecords(String name, Double res){
        records.put(name, res);
        //List list = new ArrayList(records.entrySet());
       // Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {
       //     @Override
        //    public int compare(Map.Entry<String, Double> a, Map.Entry<String, Double>b) {
           //     return a.getValue() - b.getValue();
            //}
       //});
        //printValues();
    }
    private void  printValues() {
        File high = new File("C:\\mainesweeper\\src\\ru\\nsu\\ccfit\\lusnikov\\minesweeper\\resourses\\high.txt");
        FileWriter writer;
        int i = 1;
        try{
            writer = new FileWriter(high);
            for(Map.Entry<String, Double> pair : records.entrySet())
            {
                writer.write(i+" "+pair.getKey()+ " "+ pair.getKey());
                i++;
            }
        }
        catch (IOException e){

        }

    }
    public void setStage(Stage s){
        stage = s;
        notify_all();
    }
    public void reset(){
        timeWin = new Double(-1);
        isWin = 0;
        size = getSize();
        mineCount = getMineCount();
        stage = Stage.MENAU;
        field = new Cell[size*size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                field[coord(j,i)] = new Cell(0, Cell.Mode.HIDE);
            }
        }
        timerThread.dispose();
        //timerThread.interrupt();
    }
    public void init(int a, int b){
        timerThread = new TimerThread(this);
        Random random = new Random();
        int j;
        for (int i = 0; i < mineCount; ) {
            int x = random.nextInt(size);
            int y = random.nextInt(size);
            if (field[coord(x, y)].getItem() != -1 && !(x == a && y == b)) {
                i++;
                field[coord(x, y)] = new Cell(-1, Cell.Mode.HIDE);
            }
        }

        for (int i = 0; i < size; i++) {
            for (j = 0; j < size; j++) {
                int countM = 0;
                if (field[coord(j, i)].getItem() != -1) {
                    if (j < size - 1) if (field[coord(j + 1, i)].getItem() == -1) countM++;
                    if (j > 0) if (field[coord(j - 1, i)].getItem() == -1) countM++;
                    if (i < size - 1) if (field[coord(j, i + 1)].getItem() == -1) countM++;
                    if (i > 0) if (field[coord(j, i - 1)].getItem() == -1) countM++;

                    if (j < size - 1 && i > 0) if (field[coord(j + 1, i -1)].getItem() == -1) countM++;
                    if (j > 0 && i < size - 1) if (field[coord(j - 1, i+1)].getItem() == -1) countM++;
                    if (i < size - 1 && j < size - 1) if (field[coord(j + 1, i + 1)].getItem() == -1) countM++;
                    if (i > 0 && j > 0) if (field[coord(j-1, i - 1)].getItem() == -1) countM++;

                    //System.out.println("(" + i + "," + j + ")" + countM + " ");
                    field[coord(j, i)] = new Cell(countM, Cell.Mode.HIDE);
                }
            }
        }
        timerThread.set();
        timerThread.start();
        stage = Stage.NPUT;
        notify_all();
    }
    public void fn(int x, int y){
        if (field[coord(x,y)].getVisible() == Cell.Mode.FLAG) return;
        startFonNeimanWave(x,y);
        notify_all();
    }
    public void setFlag(int x, int y){
        Integer item = field[coord(x,y)].getItem();
        if ( field[coord(x,y)].getVisible() == Cell.Mode.HIDE)
             field[coord(x,y)] = new Cell(item, Cell.Mode.FLAG);
        else if (field[coord(x,y)].getVisible() == Cell.Mode.FLAG)
            field[coord(x,y)] = new Cell(item, Cell.Mode.HIDE);
        if (check() == 0) {
            isWin = 1;
            timeWin = getTime();
           timerThread.dispose();

            stage  = Stage.PUTNAME;
        }
        notify_all();
    }
    private void loadScoreTable(){
        File f = new File("C:\\mainesweeper\\src\\ru\\nsu\\ccfit\\lusnikov\\minesweeper\\resourses\\high.txt");
        Scanner scanner = new Scanner("default");
        try{
            scanner = new Scanner(f);
        }
        catch (Exception e){}
       while(scanner.hasNext()){
          List<String> list =  Arrays.asList(scanner.nextLine().split(" +"));
         // records.put(list.get(1), Double.parseDouble(list.get(2)));
          record.table.add(new HighScoreCell(list.get(1), Float.parseFloat(list.get(2)), Integer.parseInt(list.get(3)))) ;
       }
       scanner.close();
    }
    private void startFonNeimanWave(int x, int y){
        if (field[coord(x,y)].getItem() == -1) {
            timerThread.dispose();
            isWin = -1;
            stage = Stage.ENDGAME;
            return;
        }
        field[coord(x,y)].reverseBeh(Cell.Mode.SEE);
        if (field[coord(x,y)].getItem() != 0) return;
        if (x > 0) {
            if (field[coord(x - 1,y)].getVisible() == Cell.Mode.HIDE)
                startFonNeimanWave(x-1, y);
        }
        if (x < size - 1) {
            if (field[coord(x +1,y)].getVisible() == Cell.Mode.HIDE)
                startFonNeimanWave(x+1, y);
        }
        if (y > 0 ) {
            if (field[coord(x ,y-1)].getVisible() == Cell.Mode.HIDE)
                startFonNeimanWave(x, y-1);
        }
        if (y < size - 1) {
            if (field[coord(x ,y+1)].getVisible() == Cell.Mode.HIDE)
                startFonNeimanWave(x, y+1);
        }
    }
    private int coord(int x, int y){return y*size + x;}
    public int getSize(){ return size;}
    public  Double getTimeWin(){ return timeWin;}
    public Stage getStage(){ return stage;}
    public int getMineCount(){ return mineCount;}
    public byte getWin(){return isWin;}
    @Override
    public void sub(ModelSubscriber sub){
        subs.add(sub);
        notify(sub);
    }
    public void unsub(ModelSubscriber sub){
            subs.remove(sub);
    }
    public void notify(ModelSubscriber sub) {
        subs.get(subs.indexOf(sub)).notification();
    }
    public void notify_all() {
        for (ModelSubscriber sub : subs){
            notify(sub);
        }
    }
    public void notify_all_grafic() {
        subs.get(1).notification();
    }
}
