package ru.nsu.ccfit.lusnikov.minesweeper.Controller;
import ru.nsu.ccfit.lusnikov.minesweeper.Model.Model;

import java.util.List;

public class Controller implements IController<Controller.O> {
    private Model ModelReceptionist;
    public enum O{
        EDIT
    }
    public Controller(Model m){
        ModelReceptionist = m;
    }
    public void execute(O operation,List<String> properties){
        switch(ModelReceptionist.getStage()){
            case MENAU:
                setStage(properties);
                break;
            case ABOUT:
                setStage(properties);
                break;
            case HIGHSCORE:
                setStage(properties);
                break;
            case FPUT:
                init(properties);
                break;
            case NPUT:
                editCells(properties);
                break;
            case ENDGAME:
                reset(properties);
                break;
            case PUTNAME:
                putName(properties);
                break;
        }
    }
    private void init(List<String> properties){
        ModelReceptionist.init(Integer.parseInt(properties.get(0)),Integer.parseInt(properties.get(1)));
        ModelReceptionist.fn(Integer.parseInt(properties.get(0)),Integer.parseInt(properties.get(1)));
    }
    private void editCells(List<String> properties){
        if (properties.get(0).equals("flag")) {
            ModelReceptionist.setFlag(Integer.parseInt(properties.get(1)),Integer.parseInt(properties.get(2)));
        }
        else ModelReceptionist.fn(Integer.parseInt(properties.get(0)),Integer.parseInt(properties.get(1)));
    }
    private  void setStage(List<String> properties){
        switch (properties.get(0)){
            case "1":
                ModelReceptionist.setStage(Model.Stage.FPUT);
                break;
            case "2":
                ModelReceptionist.setStage(Model.Stage.ABOUT);
                break;
            case "4":
                ModelReceptionist.setStage(Model.Stage.EXIT);
                break;
            case "3":
                ModelReceptionist.setStage(Model.Stage.HIGHSCORE);
                break;
            case "menu":
                ModelReceptionist.setStage(Model.Stage.MENAU);
                break;
        }

    }
    private  void reset(List<String> properties){
        ModelReceptionist.reset();
        switch (properties.get(0)){
            case "yes":
                ModelReceptionist.setStage(Model.Stage.FPUT);
                break;
            case "no":
                ModelReceptionist.setStage(Model.Stage.MENAU);
                break;
        }
    }
    private void putName(List<String> properties){

        ModelReceptionist.addRecords(properties.get(0), ModelReceptionist.getTimeWin());
        ModelReceptionist.setStage(Model.Stage.ENDGAME);
    }
}
