package ru.nsu.ccfit.lusnikov.minesweeper.Controller;

import java.util.List;

public interface IController<O> {
    void execute(O operation,  List<String> properties);
}
