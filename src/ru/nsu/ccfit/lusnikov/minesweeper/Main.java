package ru.nsu.ccfit.lusnikov.minesweeper;

import ru.nsu.ccfit.lusnikov.minesweeper.Model.Model;
import ru.nsu.ccfit.lusnikov.minesweeper.Viewer.Viewer;
import ru.nsu.ccfit.lusnikov.minesweeper.gui.ViewerG;
import ru.nsu.ccfit.lusnikov.minesweeper.tui.ViewerT;

public class Main {

    public static void main(String[] args) {
        Model m = new Model(9, 8);
        ViewerT v = new ViewerT(m);
        ViewerG g = new ViewerG(m);
        ViewerThread vt2 = new ViewerThread(g,m);
        ViewerThread vt1 = new ViewerThread(v,m);
        vt2.start();
        vt1.start();

    }

}
