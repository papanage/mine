package ru.nsu.ccfit.lusnikov.minesweeper;

import ru.nsu.ccfit.lusnikov.minesweeper.Model.Model;
import ru.nsu.ccfit.lusnikov.minesweeper.Viewer.Viewer;

public class ViewerThread extends Thread{
    Viewer viewer;
    Model model;
    ViewerThread(Viewer v, Model model){
        viewer = v;
        this.model = model;
    }
    @Override
    public  void run(){
        while(model.getStage() != Model.Stage.EXIT) {
            viewer.getMessage();
        }
    }

}
