package ru.nsu.ccfit.lusnikov.minesweeper.Viewer;

import ru.nsu.ccfit.lusnikov.minesweeper.Controller.Controller;

import java.util.List;

public abstract class Viewer extends BaseView{
    protected Controller controller = new Controller(publisher);
    protected void edit(List<String> properties){
        controller.execute(Controller.O.EDIT, properties);
    }
    protected abstract void update();
    protected abstract void showMessage(String message);
    public abstract int getMessage();
}
