package ru.nsu.ccfit.lusnikov.minesweeper.Viewer;

import ru.nsu.ccfit.lusnikov.minesweeper.Model.Model;
import ru.nsu.ccfit.lusnikov.minesweeper.Observer.ModelSubscriber;

public abstract class BaseView implements ModelSubscriber {
    protected Model publisher;
    protected Model getModel() {
        return publisher;
    }
    private void subscribe() {
        if (publisher != null)
            publisher.sub(this);
    }
}
