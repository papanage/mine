package ru.nsu.ccfit.lusnikov.minesweeper.Observer;

import java.util.ArrayList;

public abstract class Publisher {
    protected ArrayList<ModelSubscriber> subs = new ArrayList<>();
    protected abstract void notify(ModelSubscriber sub);
    public abstract void sub(ModelSubscriber sub);
    public abstract void unsub(ModelSubscriber sub);
}
