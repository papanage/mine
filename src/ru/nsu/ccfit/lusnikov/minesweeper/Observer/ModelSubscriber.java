package ru.nsu.ccfit.lusnikov.minesweeper.Observer;

public interface ModelSubscriber {
    void notification();
}
