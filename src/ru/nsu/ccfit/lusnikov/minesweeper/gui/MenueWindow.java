package ru.nsu.ccfit.lusnikov.minesweeper.gui;

import ru.nsu.ccfit.lusnikov.minesweeper.Model.Model;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

public class MenueWindow extends MineWindow {
    private String lastPut = "null";
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    public MenueWindow(){
        setLayout(new GridBagLayout());
        button1 = new JButton(Model.MANUE.NEW.getName());
        button2 = new JButton(Model.MANUE.ABOUT.getName());
        button3 = new JButton(Model.MANUE.HIGHSCORE.getName());
        button4 = new JButton(Model.MANUE.EXIT.getName());
        add(button1);
        add(button2);
        add(button3);
        add(button4);
        button1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                lastPut = Model.MANUE.NEW.getName();
            }
        });
        button2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                lastPut = Model.MANUE.ABOUT.getName();
            }
        });
        button3.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                System.out.print("fff");
                lastPut = Model.MANUE.HIGHSCORE.getName();
            }
        });
        button4.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                lastPut = Model.MANUE.EXIT.getName();
            }
        });

    }
    public String getS(){
        String res = Arrays.asList(lastPut.split(" +")).get(0);
        return res;

    }
    public void set(String s){
        lastPut = s;
    }

}
