package ru.nsu.ccfit.lusnikov.minesweeper.gui;

import ru.nsu.ccfit.lusnikov.minesweeper.Controller.Controller;
import ru.nsu.ccfit.lusnikov.minesweeper.Model.Model;
import ru.nsu.ccfit.lusnikov.minesweeper.Viewer.Viewer;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ViewerG extends Viewer {
    private List<String> arrayProperties = new ArrayList<>();
    MenueWindow m = new MenueWindow();
    AboutWindow aboutWindow = new AboutWindow();
    FieldWindow fw;
    HighScoreWindow hsw = new HighScoreWindow();
    public ViewerG(Model publisher){
        fw = new FieldWindow(publisher.getSize());
        this.publisher = publisher;
        controller = new Controller(publisher);
        publisher.sub(this);
    }
    private void showField(){
        hsw.setVisible(false);
        m.setVisible(false);
        aboutWindow.setVisible(false);
        fw.update(publisher);
        fw.setVisible(true);

    }
    private void showAbout(){
        m.setVisible(false);
        fw.setVisible(false);
        hsw.setVisible(false);
        aboutWindow.setVisible(true);
    }
    private void showHigh(){
        m.setVisible(false);
        fw.setVisible(false);
        aboutWindow.setVisible(false);
        hsw.setVisible(true);
    }
    private void showManue(){
        hsw.setVisible(false);
        fw.setVisible(false);
        aboutWindow.setVisible(false);
        m.setVisible(true);
    }
    @Override
    protected  void update(){
        switch(publisher.getStage()) {
            case MENAU:
                showManue();
                break;
            case ABOUT:
                showAbout();
                break;
            case HIGHSCORE:
                showHigh();
                break;
            case FPUT:
                showField();
                break;
            case NPUT:
                showField();
                break;
            case ENDGAME:
                showField();
                break;
            case PUTNAME:
                showField();
                break;
        }
    }
    @Override
    protected void showMessage(String message){}
    @Override
    public int getMessage(){
        switch(publisher.getStage()) {
            case MENAU:
                String s = m.getS();
                if (!s.equals("null")){
                    arrayProperties = Arrays.asList(s.split(""));
                    edit(arrayProperties);
                    m.set("null");
                }
                break;
            case ABOUT:
                boolean igm = aboutWindow.isGoMenue();
                if (igm){
                    arrayProperties = Arrays.asList("menu ".split(" "));
                    edit(arrayProperties);
                    aboutWindow.setzero();
                }
                break;
            case HIGHSCORE:
                boolean igm1 = hsw.isGoMenue();
                if (igm1){
                    arrayProperties = Arrays.asList("menu ".split(" "));
                    edit(arrayProperties);
                    hsw.setzero();
                }
                break;
            case FPUT:
                arrayProperties = fw.getLastCoord();
                if (arrayProperties != null){
                    edit(arrayProperties);
                }
                break;
            case NPUT:
                arrayProperties = fw.getLastCoord();
                if (arrayProperties != null){
                    edit(arrayProperties);
                }
                break;
            case PUTNAME:
                String name = JOptionPane.showInputDialog(fw,
                        "Put your name", "Win "+publisher.getTimeWin() + " sec",JOptionPane.INFORMATION_MESSAGE);
                if (name != null) edit(Arrays.asList(name.split(" ")));
                else edit(null);
                break;
            case ENDGAME:
                int answer = JOptionPane.showConfirmDialog(fw,
                        "Do you wanna start new GAME?", "TOTO",JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
                if (answer != 1)
                    edit(Arrays.asList("yes".split(" ")));
                else
                    edit(Arrays.asList("no".split(" ")));
                break;
        }
        return 0;
    }
    @Override
    public void notification(){
        update();
    }

}
