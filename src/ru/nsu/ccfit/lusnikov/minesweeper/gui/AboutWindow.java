package ru.nsu.ccfit.lusnikov.minesweeper.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Scanner;

public class AboutWindow extends MineWindow {
    boolean goMenue = false;
    public AboutWindow(){
        Scanner scanner = new Scanner("ff");
        File f = new File("C:\\mainesweeper\\src\\ru\\nsu\\ccfit\\lusnikov\\minesweeper\\resourses\\about.txt");
        try{
            scanner = new Scanner(f);
        }
        catch (Exception e){}
        setLayout(new GridBagLayout());
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        panel.setPreferredSize(new Dimension(700, 600));
        JButton toMenue = new JButton("Назад");
        int i = 0;
        while (scanner.hasNext()) {
            Font font = new Font("TimesRoman", Font.BOLD, 15);
            JLabel about = new JLabel(scanner.nextLine());
            about.setFont(font);
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridy = i++;
            panel.add(about, gbc);
        }
        add(panel);
        panel.add(toMenue);

        toMenue.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                 goMenue = true;
              }
          });


    }
    public boolean isGoMenue(){
        return goMenue;
    }
    public void setzero(){
        goMenue = false;
    }
}
