package ru.nsu.ccfit.lusnikov.minesweeper.gui;

import ru.nsu.ccfit.lusnikov.minesweeper.Model.Cell;
import ru.nsu.ccfit.lusnikov.minesweeper.Model.Model;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.util.List;


public class FieldWindow extends MineWindow{
    int size;
    private ImageIcon invis, bomb, one, two, flag, empty, redbobm, bluebomb;
    private final int b_x = 50;
    private final int b_y = 50;
    private JPanel south, east;
    private List<String> lastCoord;
    public FieldWindow(int size){
        this.size = size;
       lastCoord = null;
       initIm();
       setLayout(new GridBagLayout());
       south = new JPanel();
       south.setPreferredSize(new Dimension(size*(b_x + 10), size*(b_y + 10)));
       south.setBorder(BorderFactory.createLineBorder(Color.black));
       GridBagConstraints gbc = new GridBagConstraints();
       gbc.anchor = GridBagConstraints.SOUTH;
       gbc.insets.right = 10;
       gbc.gridy = 1;
       add(south, gbc);
       east = new JPanel();
       east.setPreferredSize(new Dimension(150, 50));
       east.setBorder(BorderFactory.createLineBorder(Color.black));
       GridBagConstraints gbc2 = new GridBagConstraints();
       gbc2.anchor = GridBagConstraints.NORTH;
       add(east, gbc2);
       for (int i = 0;i < size; i++){
           for (int j =0; j < size; j++) {
               JButton button = new JButton(empty);
               button.setPreferredSize(new Dimension(b_x, b_y));
               button.addMouseListener(new MouseL(i,j, this));
               south.add(button);
           }
       }

   }
    public void setLastCoord(List<String> a){
       lastCoord = a;
   }
    public List<String> getLastCoord(){
        return lastCoord;
   }
    public void update(Model m){
       south.removeAll();
       east.removeAll();
       Font f = new Font("TimesRoman", Font.BOLD, 20);
       JLabel timer = new JLabel("Время:"+Integer.toString(m.getTime().intValue())+ "сек.");
       timer.setFont(f);
       east.add(timer);
       lastCoord = null;
       for (int i = 0;i < size; i++){
           for (int j =0; j < size; j++) {
               ImageIcon im = chose(j,i,m);
               JButton button;
               if (im == null){
                   button = new JButton(m.getViewOfCell(j,i).get().toString());
                   Font BigFontTR = new Font("TimesRoman", Font.BOLD, 20);
                   button.setFont(BigFontTR);
               }
               else if (im == redbobm){
                    button = new JButton(bomb);
                    button.setBackground(Color.RED);
               }
               else if (im == bluebomb){
                   button = new JButton(bomb);
                   button.setBackground(Color.BLUE);
               }
               else button = new JButton(im);
               button.setPreferredSize(new Dimension(b_x, b_y));
               button.addMouseListener(new MouseL(j,i, this));
               south.add(button);
           }
       }
   }

    private ImageIcon chose(int x, int y, Model m){
       ImageIcon s = null;
       Model.BEH b = m.getViewOfCell(x,y);
       switch (b) {
           case BOMB:
               s = bomb;
               break;
           case FLAG:
               s = flag;
               break;
           case INVIS:
               s = empty;
               break;
           case NUMBER:
               if (b.get() == 0) s = invis;
               break;
           case UNFLAGBOMB:
               s = redbobm;
               break;
           case FLAGBOMB:
               s = bluebomb;
               break;
       }
    return s;
   }
    private void initIm(){
       invis = new ImageIcon("C:\\mainesweeper\\src\\ru\\nsu\\ccfit\\lusnikov\\minesweeper\\resourses\\invis.png");
       Image img = invis.getImage() ;
       Image newimg = img.getScaledInstance( b_x, b_y,  java.awt.Image.SCALE_SMOOTH ) ;
       invis = new ImageIcon(newimg);

       bomb = new ImageIcon("C:\\mainesweeper\\src\\ru\\nsu\\ccfit\\lusnikov\\minesweeper\\resourses\\bomb.jpg");
       img = bomb.getImage() ;
       newimg = img.getScaledInstance( b_x, b_y,  java.awt.Image.SCALE_SMOOTH ) ;
       bomb = new ImageIcon(newimg);

       one = new ImageIcon("C:\\mainesweeper\\src\\ru\\nsu\\ccfit\\lusnikov\\minesweeper\\resourses\\1.png");
       img = one.getImage() ;
       newimg = img.getScaledInstance( b_x, b_y,  java.awt.Image.SCALE_SMOOTH ) ;
       one = new ImageIcon(newimg);

       flag = new ImageIcon("C:\\mainesweeper\\src\\ru\\nsu\\ccfit\\lusnikov\\minesweeper\\resourses\\flag.jpg");
       img = flag.getImage() ;
       newimg = img.getScaledInstance( b_x, b_y,  java.awt.Image.SCALE_SMOOTH ) ;
       flag = new ImageIcon(newimg);

       empty = new ImageIcon("C:\\mainesweeper\\src\\ru\\nsu\\ccfit\\lusnikov\\minesweeper\\resourses\\emty.jpg");
       img = empty.getImage() ;
       newimg = img.getScaledInstance( b_x, b_y,  java.awt.Image.SCALE_SMOOTH ) ;
       empty = new ImageIcon(newimg);

       two = new ImageIcon("C:\\mainesweeper\\src\\ru\\nsu\\ccfit\\lusnikov\\minesweeper\\resourses\\two.png");
       img = two.getImage() ;
       newimg = img.getScaledInstance( b_x, b_y,  java.awt.Image.SCALE_SMOOTH ) ;
       two = new ImageIcon(newimg);

       redbobm = new ImageIcon();
       bluebomb = new ImageIcon();
   }

}
