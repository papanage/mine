package ru.nsu.ccfit.lusnikov.minesweeper.gui;

import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class MouseL extends MouseAdapter {
    private List<String> arrayProperties = new ArrayList<>();
    FieldWindow fw;
    public MouseL(Integer x, Integer y, FieldWindow fw){
        arrayProperties.add(x.toString());
        arrayProperties.add(y.toString());
        this.fw = fw;
    }
    public void mouseClicked(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON3) {
            arrayProperties.add(0,"flag");
            fw.setLastCoord(arrayProperties);
        }
        if (e.getButton() == MouseEvent.BUTTON1) {
            fw.setLastCoord(arrayProperties);
        }
    }
}
