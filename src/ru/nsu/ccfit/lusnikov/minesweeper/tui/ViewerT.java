package ru.nsu.ccfit.lusnikov.minesweeper.tui;

import ru.nsu.ccfit.lusnikov.minesweeper.Controller.Controller;
import ru.nsu.ccfit.lusnikov.minesweeper.Model.Model;
import ru.nsu.ccfit.lusnikov.minesweeper.Viewer.Viewer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ViewerT extends Viewer{
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private List<String> arrayProperties = new ArrayList<>();
    @Override
    protected void update(){
        switch(publisher.getStage()){
            case MENAU:
                showManue();
                break;
            case FPUT:
                showField();
                break;
            case NPUT:
                showField();
                break;
            case PUTNAME:
                showMessage("CONGRATILUTIONS, you are winning!\nPut your name");
                break;
            case ENDGAME:
                if (publisher.getWin() == -1) showMessage("SORRY");
                showMessage("Can you start new game?");
                break;
        }
    }
    private void showField(){
        for (int i = 0; i < publisher.getSize() + 1; i++){
            for (int j = 0; j < publisher.getSize() + 1; j++){
                if (i == 0 && j == 0) System.out.print("k ");
                else if (i == 0) System.out.print(j-1 + " ");
                else if (j == 0) System.out.print(i-1 + " ");
                else showCell(j - 1, i - 1);
            }
            System.out.println();
        }
       // System.out.println("\n\n\n");
    }
    private void showManue(){
        showMessage(Model.MANUE.NEW.getName());
        showMessage(Model.MANUE.ABOUT.getName());
        showMessage(Model.MANUE.HIGHSCORE.getName());
        showMessage(Model.MANUE.EXIT.getName());
    }
    @Override
    public int getMessage(){
        try {
            String line;
            if ((line = reader.readLine()) != null)
            arrayProperties = Arrays.asList(line.split(" +"));
            edit(arrayProperties);


        } catch (IOException e) { }
        return 0;
    }
    @Override
    public void showMessage(String message){ System.out.println(message);}
    private void showCell(int x, int y){
        String s;
        switch (publisher.getViewOfCell(x,y)) {
            case BOMB:
                s = "@";
                break;
            case FLAG:
                s = "f";
                break;
            case INVIS:
                s = "#";
                break;
            default:
                s = publisher.getViewOfCell(x, y).get().toString();
        }
       System.out.print(s + " ");
    }
    public void notification(){
        update();
    }
    public ViewerT(Model publisher){
        this.publisher = publisher;
        controller = new Controller(publisher);
        publisher.sub(this);
    }
}
